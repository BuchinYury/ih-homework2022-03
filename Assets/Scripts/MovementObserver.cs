using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementObserver : MonoBehaviour
{
    public List<Vector3> Movements { get; private set; } = new List<Vector3>();

    public void OnMove(Vector3 position)
    {
        Movements.Add(position);
    }
}