
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsMover : MonoBehaviour
{
    [SerializeField] private Transform[] _observablePoints;
    [SerializeField] private float _lookDelay;

    [SerializeField] private LayerMask _obstacleMask;

    private float _timer = 0f;
    private int _currentObservablePointIndex = 0;

    private void Awake()
    {
        _timer = _lookDelay;
    }

    private void Update()
    {
        LookAtTimerTick();
        LookAtPoint(_currentObservablePointIndex);
    }

    private void LookAtTimerTick()
    {
        _timer -= Time.deltaTime;

        if (_timer <= 0)
        {
            int nextPointIndex = _currentObservablePointIndex + 1;

            if (nextPointIndex >= _observablePoints.Length)
                nextPointIndex = 0;

            _currentObservablePointIndex = nextPointIndex;
            _timer = _lookDelay;
        }
    }

    private void LookAtPoint(int pointIndex)
    {
        Vector3 moveToPosition = _observablePoints[pointIndex].transform.position;
        transform.position = new Vector3(moveToPosition.x, transform.position.y, moveToPosition.z);
    }
}
