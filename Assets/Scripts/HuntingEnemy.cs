using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementObserver))]
[RequireComponent(typeof(MeshRenderer))]
public class HuntingEnemy : Enemy
{

    [SerializeField] private float _huntingStartAfterDelay;
    [SerializeField] private float _stepDelay;
    

    private MovementObserver _preyMovementObserver;

    private bool isStartHunting = false;

    private float _huntingStartTimer = 0f;
    private float _stepTimer = 0f;
    private int _movementStep = 0;

    private MeshRenderer _meshRenderer;

    // Start is called before the first frame update
    void Awake()
    {
        _preyMovementObserver = GetComponent<MovementObserver>();
        _huntingStartTimer = _huntingStartAfterDelay;
        _stepTimer = _stepDelay;

        _meshRenderer = GetComponent<MeshRenderer>();
        _meshRenderer.enabled = isStartHunting;
    }
    private void Update()
    {
        if (isStartHunting)
            TryTakeStep();
        else
            TryStartHunting();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player) && isStartHunting)
        {
            player.Kill();
        }
    }

    private void TryStartHunting()
    {
        if (isStartHunting)
            return;

        _huntingStartTimer -= Time.deltaTime;

        if (_huntingStartTimer <= 0)
        {
            StartHunting();
        }
    }

    private void StartHunting()
    {
        isStartHunting = true;
        _meshRenderer.enabled = isStartHunting;
    }

    private void TryTakeStep()
    {
        if (isStartHunting == false)
            return;

        _stepTimer -= Time.deltaTime;

        if (_stepTimer <= 0)
        {
            TakeStep();
            _stepTimer = _stepDelay;
        }
    }

    private void TakeStep()
    {
        List<Vector3> movements = _preyMovementObserver.Movements;
        if (movements.Count > _movementStep)
        {
            Vector3 step = movements[_movementStep];
            MoveTo(step);
            _movementStep = _movementStep + 1;
        }
    }

    private void MoveTo(Vector3 position)
    {
        transform.position = new Vector3(position.x, transform.position.y, position.z);
    }
}
