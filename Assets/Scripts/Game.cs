using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private float _initialTimerValue;
    [SerializeField] private Text _timerView;

    [Header("Objects")]
    [SerializeField] private Player _player;
    [SerializeField] private Exit _exit;

    private float _timer = 0;
    private bool _gameIsEnded;

    private void Awake()
    {
        SetTimerValue(_initialTimerValue);
    }

    private void Start()
    {
        _player.Enable();
        _exit.Close();
    }

    private void Update()
    {
        if (_gameIsEnded)
            return;

        TimerTick();
        TryCompleteLevel();
        LookAtPlayerHealth();
        LookAtPlayerInventory();
    }

    private void TimerTick()
    {
        SetTimerValue(_timer - Time.deltaTime);

        if (_timer <= 0)
            Lose();
    }

    private void TryCompleteLevel()
    {
        if(_exit.IsOpen == false)
            return;
            
        if (FlatPosition(_exit.transform) == FlatPosition(_player.transform))
            Victory();
    }

    private void LookAtPlayerHealth()
    {
        if (_player.IsAlive)
            return;

        Lose();
        Destroy(_player.gameObject);
    }

    private void Lose()
    {
        _gameIsEnded = true;
        _player.Disable();
    }

    private void Victory()
    {
        _gameIsEnded = true;
        _player.Disable();
    }

    private void LookAtPlayerInventory()
    {
        if (_player.HasKey)
            _exit.Open();
    }

    private Vector2 FlatPosition(Transform transform)
    {
        return new Vector2(transform.position.x, transform.position.z);
    }

    private void SetTimerValue(float value)
    {
        _timer = value;
        _timerView.text = $"{value:F1}";
    }
}
